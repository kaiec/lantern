from django.apps import AppConfig


class LanternConfig(AppConfig):
    name = 'lantern'
