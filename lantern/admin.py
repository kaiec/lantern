from django.contrib import admin
from . import models


admin.site.register(models.Profile)
admin.site.register(models.Contact)
admin.site.register(models.Forum)
admin.site.register(models.Post)
