"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf import settings

from . import views



urlpatterns = [
    path('login', auth_views.LoginView.as_view(template_name='lantern/login.html', extra_context={'title':settings.LANTERN_TITLE, 'login_message': settings.LANTERN_LOGIN_MESSAGE}), name='login'),
    path('password_reset', auth_views.PasswordResetView.as_view(template_name='lantern/password_reset.html', from_email=settings.EMAIL_FROM, extra_context={'title':settings.LANTERN_TITLE}), name='password_reset'),
    path('password_reset_done', auth_views.PasswordResetView.as_view(template_name='lantern/password_reset_done.html', extra_context={'title':settings.LANTERN_TITLE}), name='password_reset_done'),
    path('password_reset_confirm/<str:uidb64>/<str:token>', auth_views.PasswordResetConfirmView.as_view(template_name='lantern/password_reset_confirm.html', extra_context={'title':settings.LANTERN_TITLE}), name='password_reset_confirm'),
    path('password_reset_complete', auth_views.PasswordResetCompleteView.as_view(template_name='lantern/password_reset_complete.html', extra_context={'title':settings.LANTERN_TITLE}), name='password_reset_complete'),
    path('password/', auth_views.PasswordChangeView.as_view(template_name='lantern/password_change.html', extra_context={'title':settings.LANTERN_TITLE, 'menu': settings.LANTERN_MENU}), name='password_change'),
    path('password_change_done', auth_views.PasswordChangeDoneView.as_view(template_name='lantern/password_change_done.html', extra_context={'title':settings.LANTERN_TITLE, 'menu': settings.LANTERN_MENU}), name='password_change_done'),
    path('logout', auth_views.LogoutView.as_view(template_name='lantern/logout.html', extra_context={'title':settings.LANTERN_TITLE}), name='logout'),
    path('registration', views.registration, name='registration'),
    path('edit-profile', views.edit_profile, name='edit-profile'),
    path('last-logins', views.last_logins, name='last-logins'),
    path('not-seen', views.not_seen, name='not-seen'),
    path('all-users', views.all_users, name='all-users'),
    path('details/<str:user_id>', views.details, name='details'),
    path('forum/<str:forum>/<int:page>', views.forum, name='forum-page'),
    path('forum/<str:forum>', views.forum, name='forum'),
    path('post/<int:post_id>', views.view_post, name='view-post'),
    path('edit-post/<int:post_id>/<str:origin>', views.edit_post, name='edit-post'),
    path('reply-post/<int:post_id>/<str:origin>', views.reply_post, name='reply-post'),
    path('archive', views.list_archived_forums, name='archive'),
    path('', views.index, name='index'),
]
