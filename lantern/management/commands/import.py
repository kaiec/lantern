from csv import DictReader
from lantern.models import User, Profile, Contact, Forum, Post
import datetime
import html
from django.db.models import Q
from django.core.management.base import BaseCommand, CommandError

def import_users():

    def split_name(name):
        if not "(" in name:
            return name.strip(), ""
        current = name.split('(')[0].strip()
        start = name.find('(geb. ')
        end = name.find(')', start+6)
        birthname = name[start+6:end].strip()
        return current, birthname
    
    with open ('old-data/csv/hi_users.csv') as f:
        reader = DictReader(f)
        for olduser in reader:
            if olduser['freischaltung'] == '0':
                continue
            User.objects.filter(username=olduser['login']).delete()
            user = User()
            user.username = olduser['login'].strip()
            user.password = olduser['pass'].strip()
            user.email = olduser['email'].strip()
            lastname, birthname = split_name(olduser['nachname'].strip())
            user.first_name = olduser['vorname'].strip()
            user.last_name = lastname
            user.last_login = datetime.datetime.strptime(olduser['lastlogin'], "%Y-%m-%d %H:%M:%S")
            user.save()
            profile = Profile()
            profile.user = user
            profile.birthname = birthname
            profile.address = "{}\n{} {}".format(olduser['adresse'], olduser['plz'], olduser['ort'])
            profile.birthdate = datetime.datetime.strptime(olduser['geburtstag'], "%Y-%m-%d")
            profile.description = olduser['beschreibung'].strip()
            profile.save()
            if olduser['telefon']:
                telefon = Contact()
                telefon.profile = profile
                telefon.value = olduser['telefon'].strip()
                telefon.name = 'Telefon'
                telefon.save()
            if olduser['fax']:
                telefon = Contact()
                telefon.profile = profile
                telefon.value = olduser['fax'].strip()
                telefon.name = 'FAX'
                telefon.save()
            if olduser['handy']:
                telefon = Contact()
                telefon.profile = profile
                telefon.value = olduser['handy'].strip()
                telefon.name = 'Mobil'
                telefon.save()
            print("Imported: {}".format(user))


def import_posts():

    def split_name(name):
        if not "(" in name:
            return html.unescape(name.strip()), ""
        current = html.unescape(name.split('(')[0].strip())
        start = name.find('(geb. ')
        end = name.find(')', start+6)
        birthname = html.unescape(name[start+6:end].strip())
        return current, birthname

    def umlauts(name):
        name = name.replace("ae", "ä").replace("oe", "ö").replace("ue", "ü")
        name = name.replace("Ae", "Ä").replace("Oe", "Ö").replace("Ue", "Ü")
        name = name.replace("ss", "ß")
        return name
    
    with open ('old-data/csv/10_jahre.csv') as f:
        reader = DictReader(f)
        Forum.objects.filter(slug="10jahre").delete()
        forum = Forum()
        forum.name = "10 Jahre"
        forum.slug = "10jahre"
        forum.save()
        
        for oldpost in reader:
            first_name = html.unescape(oldpost['name'].split(" ")[0].strip())
            last_name, birthname = split_name(oldpost['name'][oldpost['name'].find(" "):])
            fn = Q(first_name=first_name)
            ln = Q(last_name=last_name) | Q(last_name=umlauts(last_name))
            bn = Q(profile__birthname=last_name)
            matches = User.objects.filter(fn & ln | fn & bn)
            if len(matches)!=1:
                print("Ambigous match: ")
                print(oldpost['name'], first_name, last_name, birthname, matches) 
                continue
            post = Post()
            post.forum = forum
            post.user = matches[0]
            post.text = html.unescape(oldpost['text'].strip())
            post.date = datetime.datetime.strptime(oldpost['datum'], "%Y-%m-%d %H:%M:%S")
            post.save()

class Command(BaseCommand):
    def handle(self, *args, **options):
        import_posts()
