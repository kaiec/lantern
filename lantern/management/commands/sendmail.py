from lantern.models import User, Profile, Contact, Forum, Post
import datetime
from django.db.models import Q
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone
import textwrap


def wrap_text(text):
    lines = text.split('\n')
    wrapped = []
    for line in lines:
        sublines = textwrap.wrap(line, 78)
        wrapped.append(" \n".join(sublines))
    return "\n".join(wrapped)



def create_mail(posts, user):
    subject = "{}: Neue Nachrichten".format(settings.LANTERN_TITLE)
    text = ''
    if not user.last_login:
        text += '''
Wichtig: Du hast dich noch nie eingeloggt.
Bitte logge dich einmal ein, damit wir wissen, dass deine Daten korrekt sind.

        '''
    elif ((timezone.now() - user.last_login).total_seconds() > 365*24*60*60):
        text += '''
Wichtig: Du hast dich seit über einem Jahr nicht mehr eingeloggt.
Bitte logge dich einmal ein, damit wir wissen, dass deine Daten noch aktuell sind.

        '''

    text += '''
Hallo {},

es gibt neue Nachrichten auf {}. Seit dem {} ist folgendes passiert: 

----------------------------------------------------------------------

'''.format(user.first_name, settings.LANTERN_URL, user.profile.last_mail_digest.strftime("%d.%m.%Y"))
    for post in posts:
        reply = ""
        if post.reply_to:
            reply = " als Antwort auf einen Post von {}".format(post.reply_to.user.first_name + " " + post.reply_to.user.last_name)
        text += '{} im Bereich "{}"{}:\n{} {}{}\n\n'.format(
                post.date.strftime("%d.%m.%Y %H:%M Uhr"),
                post.forum.name,
                " (Wichtig)" if post.important else "",
                post.user.first_name,
                post.user.last_name,
                reply
                )
        text += post.text + '\n\n'
        text += '----------------------------------------------------------------------\n\n'
    
    text += '''
Du erhältst diese Mail, weil du dich unter {} mit der Email-Adresse {} angemeldet hast.

Dein letzter Login: {} 

Die Mails werden normalerweise verschickt, sobald eine neue Nachricht veröffentlicht wurde, die als "Wichtig" markiert wurde.

In jedem Fall wird maximal einmal am Tag eine Mail verschickt, die alle neuen Beiträge seit der letzten Mail enthält.

Wie immer gilt: Bitte halte deine Kontaktdaten, insbesondere deine E-Mail-Adresse aktuell.

Wenn du Benutzername oder Passwort vergessen hast, kannst du dir einen Link zum Erstellen eines neuen Passworts zusenden lassen.

Wenn du nicht mehr an deine Mails kommst oder andere Fragen oder Anregungen hast, melde dich einfach per Mail an {}.
'''.format(settings.LANTERN_URL, 
        user.email, 
        "Noch nie." if not user.last_login else user.last_login.strftime("%d.%m.%Y %H:%M Uhr"),
        " oder ".join(settings.LANTERN_ADMIN_EMAILS))
    return subject, wrap_text(text), settings.EMAIL_FROM, [user.email]


class Command(BaseCommand):
    def handle(self, *args, **options):
        for user in User.objects.filter(email__isnull=False, is_active=True, email__contains='@'):
            import_posts = Post.objects.filter(date__gte=user.profile.last_mail_digest, important=True)
            if len(import_posts) == 0:
                continue
            posts = Post.objects.filter(date__gte=user.profile.last_mail_digest).order_by('-important', '-date')
            mail = create_mail(posts, user)
            success = send_mail(*mail)
            if success:
                print("Mail gesendet an {}".format(user.email))
                user.profile.last_mail_digest = timezone.now()
                user.profile.save()
            else:
                print("Mail error: {}".format(user.email))
