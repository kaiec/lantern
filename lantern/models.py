from django.db import models
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.forms import ModelForm
from django.db.models import Q
from django.utils import timezone
from csv import DictReader
import html
import datetime


class Forum(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    write_group = models.ForeignKey(Group, on_delete=models.DO_NOTHING, null=True, blank=True)
    is_archived = models.BooleanField(default=False)
    is_writable = models.BooleanField(default=True)
    is_listed = models.BooleanField(default=True)

    def is_user_writable(self, user):
        return self.is_writable and (self.write_group==None or self.write_group in user.groups.all())

    def first_last_post(self):
        posts = self.post_set.all().order_by('date')
        if len(posts)==0:
            return None
        return posts[0].date, posts[len(posts)-1].date  

    def __str__(self):
        return "{} ({})".format(self.name, self.slug)


class NotificationSetting(models.Model):
    ALL = 'ALL'
    IMPORTANT = 'IMPORTANT'
    NONE = 'NONE'
    NOTIFICATIION_CHOICES = [
            (ALL, 'Alle Nachrichten'),
            (IMPORTANT, 'Wichtige Nachrichten'),
            (NONE, 'Keine Nachrichten'),
            ]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    forum = models.ForeignKey(Forum, on_delete=models.CASCADE, blank=True, null=True)
    value = models.CharField(max_length=20, choices=NOTIFICATIION_CHOICES, default=IMPORTANT)

    def __str__(self):
        return "{} - {}: {}".format(self.user, self.forum, self.value)


def NotificationSettingForm(ModelForm):
    class Meta:
        model = NotificationSetting
        fields = ['value']


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthname = models.CharField(max_length=200, blank=True)
    address = models.TextField(blank=True)
    birthdate = models.DateField(null=True, blank=True)
    description = models.TextField(blank=True)
    last_mail_digest = models.DateTimeField(default=timezone.now)
    default_notification = models.CharField(max_length=20, choices=NotificationSetting.NOTIFICATIION_CHOICES, default=NotificationSetting.IMPORTANT)

    def __str__(self):
        return "Profile for {}".format(self.user)


class UserForm(UserChangeForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']


class NewUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['birthname', 'address', 'birthdate', 'description']
        labels = {'birthname': 'Geburtsname', 'address': 'Adresse', 'birthdate': 'Geburtstag', 'description': 'Über dich'}


class Contact(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    value = models.CharField(max_length=100)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{} ({}), {}".format(self.value, self.name, self.profile.user.username)


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'value']
        labels = {'name': 'Art', 'value': 'Wert'}
        help_texts = {'name': 'z.B. Handy, Skype, Signal, Website, LinkedIn, ...'}


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    forum = models.ForeignKey(Forum, on_delete=models.CASCADE)
    reply_to = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name='reply_to_set')
    date = models.DateTimeField()
    text = models.TextField()
    important = models.BooleanField(default=False)


    def replies(self):
        res = self.reply_to_set.all().order_by('date')
        return res


    def is_editable(self):
        return (timezone.now() - self.date).total_seconds() < 15 * 60


    def forum_page(self):
        for index, post in enumerate(self.forum.post_set.filter(reply_to__isnull=True).order_by('-date')):
            if self.id == post.id:
                return (index // 10) + 1

    def __str__(self):
        return "{} ({}): {}".format(self.date, self.user, self.text[:100])


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['text', 'important']
        labels = {'text': 'Nachricht', 'important': 'Wichtige Nachricht'}
        help_texts = {'important': 'Wichtige Nachrichten werden an alle Nutzer per E-Mail geschickt, die das nicht deaktiviert haben.'}
    
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)




    
