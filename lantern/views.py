from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.forms import inlineformset_factory
from django.conf import settings
from django.contrib import messages
from django.core.paginator import Paginator
from django.core.mail import send_mail
from django.utils import timezone
from django.urls import reverse
from itertools import chain

from . import models

def get_context():
    context = {}
    context['title'] = settings.LANTERN_TITLE
    context['menu'] = settings.LANTERN_MENU
    return context


def index(request):
    return redirect(settings.LANTERN_HOME)


def registration(request):
    context = get_context()
    user = models.User()
    profile = models.Profile()
    context['userform'] = models.NewUserForm(instance=user)
    context['profileform'] = models.ProfileForm(instance=profile)
    ContactFormSet = inlineformset_factory(models.Profile, models.Contact, form=models.ContactForm, extra=2)
    context['contactform'] = ContactFormSet(instance=profile, initial=[{'name': 'Telefon', 'value': ''}])
    if request.POST:
        context['userform'] = models.NewUserForm(request.POST, instance=user)
        context['profileform'] = models.ProfileForm(request.POST, instance=profile)
        context['contactform'] = ContactFormSet(request.POST, instance=profile)
        if context['userform'].is_valid() and context['profileform'].is_valid() and context['contactform'].is_valid():
            user = context['userform'].save(commit=False)
            profile = context['profileform'].save(commit=False)
            user.is_active = False
            user.save()
            user.refresh_from_db()
            profile.user = user
            profile.save()
            profile.refresh_from_db()
            for cf in context['contactform']:
                cf.instance.profile = profile
                cf.save()
            messages.success(request, 'Registrierung erfolgreich. Du wirst nach deiner Freischaltung per Mail informiert.')
            send_mail(
                    "{}: Neuer Benuter {}".format(settings.LANTERN_TITLE, user.username),
                    "Ein neuer Benutzer hat sich registriert: {}".format(user.username),
                    settings.EMAIL_FROM,
                    settings.LANTERN_ADMIN_EMAILS)
            return redirect('/login')
    return render(request, 'lantern/registration.html', context)


@login_required
def edit_profile(request):
    context = get_context()
    user = models.User.objects.get(username=request.user.username)
    context['userform'] = models.UserForm(instance=user)
    context['profileform'] = models.ProfileForm(instance=user.profile)
    ContactFormSet = inlineformset_factory(models.Profile, models.Contact, form=models.ContactForm, extra=0)
    context['contactform'] = ContactFormSet(instance=user.profile)
    if request.POST:
        context['userform'] = models.UserForm(request.POST, instance=user)
        context['profileform'] = models.ProfileForm(request.POST, instance=user.profile)
        context['contactform'] = ContactFormSet(request.POST, instance=user.profile)
        if context['userform'].is_valid() and context['profileform'].is_valid() and context['contactform'].is_valid():
            context['userform'].save()
            context['profileform'].save()
            context['contactform'].save()
            messages.success(request, 'Daten wurden aktualisiert.')
            return redirect('edit-profile')

    return render(request, 'lantern/edit-profile.html', context)


@login_required
def last_logins(request):
    context = get_context()
    context['list_name'] = 'Zuletzt gesehen'
    users = models.User.objects.filter(profile__isnull=False, is_active=True).order_by('-last_login')[:20]
    context['users'] = users
    return render(request, 'lantern/list-users.html', context)


@login_required
def not_seen(request):
    context = get_context()
    context['list_name'] = 'Lange nicht gesehen'
    missing = models.User.objects.filter(groups__name='missing')
    nomail = models.User.objects.filter(profile__isnull=False, is_active=True, email__exact="").exclude(groups__name='missing')
    users = models.User.objects.filter(profile__isnull=False, is_active=True).exclude(groups__name='missing').exclude(email__exact='').order_by('last_login')
    context['users'] = chain(missing, nomail, users)
    return render(request, 'lantern/list-users.html', context)


@login_required
def details(request, user_id):
    context = get_context()
    user = get_object_or_404(models.User, pk=user_id) 
    context['show_user'] = user
    return render(request, 'lantern/details.html', context)


@login_required
def all_users(request):
    context = get_context()
    context['list_name'] = 'Alle Benutzer'
    users = models.User.objects.filter(profile__isnull=False, is_active=True).order_by('first_name')
    context['users'] = users
    return render(request, 'lantern/list-users.html', context)


@login_required
def forum(request, forum, page=1):
    context = get_context()
    forum = get_object_or_404(models.Forum, slug=forum)
    context['writable'] = forum.is_user_writable(request.user)
    if context["writable"]:
        post = models.Post()
        post.forum = forum
        if request.POST:
            form = models.PostForm(request.POST, instance=post)
            if form.is_valid():
                post.date = timezone.now()
                post.user = request.user
                post.save()
                return redirect(reverse('forum-page', kwargs={'forum': forum.slug, 'page': page}))
            context['form'] = form
        else:
            context['form'] = models.PostForm()
    posts = forum.post_set.filter(reply_to__isnull=True).order_by("-date") 
    p = Paginator(posts, 10)
    context['forum'] = forum
    context['posts'] = p.page(page).object_list
    left = range(1, min(4, p.num_pages + 1))
    center = range(max(left[-1] + 1, page-3), min(page + 4, p.num_pages + 1))
    if len(center)>0:
        right = range(max(center[-1] + 1, p.num_pages-2), p.num_pages + 1)
    else:
        right = []
    context['pages_left'] = left
    context['pages_center'] = center
    context['pages_right'] = right
    context['sep_left'] = center and center[0] - left[-1] > 1
    context['sep_right'] = center and right and right[0] - center[-1] > 1
    context['current'] = page
    return render(request, 'lantern/forum.html', context)


@login_required
def view_post(request, post_id):
    context = get_context()
    context["post"] = get_object_or_404(models.Post, pk=post_id)
    return render(request, 'lantern/view-post.html', context)


@login_required
def edit_post(request, post_id, origin='forum'):
    context = get_context()
    context["post"] = get_object_or_404(models.Post, pk=post_id)
    context["writable"] = context["post"].forum.is_user_writable(request.user)
    if not context["writable"] or request.user != context['post'].user or not context['post'].is_editable():
        return redirect(reverse('view-post', kwargs={'post_id':post_id}))
    if request.POST:
        context['form'] = models.PostForm(request.POST, instance=context['post'])
        if context['form'].is_valid():
            context['form'].save()
            return redirect(reverse('forum-page', kwargs={'forum': context['post'].forum.slug, 'page': context['post'].forum_page() }))
    else:
        context['form'] = models.PostForm(instance=context['post'])
    return render(request, 'lantern/edit-post.html', context)


@login_required
def reply_post(request, post_id, origin='forum'):
    context = get_context()
    context["post"] = get_object_or_404(models.Post, pk=post_id)
    context["writable"] = context["post"].forum.is_user_writable(request.user)
    if not context["writable"]:
        return redirect(reverse('view-post', kwargs={'post_id':post_id}))
    if request.POST:
        context['form'] = models.PostForm(request.POST)
        if context['form'].is_valid():
            reply = context['form'].save(commit=False)
            reply.date = timezone.now()
            reply.user = request.user
            reply.reply_to = context['post']
            reply.forum = context['post'].forum
            reply.save()
            return redirect(reverse('forum-page', kwargs={'forum': context['post'].forum.slug, 'page': context['post'].forum_page() }))
    else:
        context['form'] = models.PostForm()
        del context['form'].fields['important']
    return render(request, 'lantern/reply-post.html', context)


@login_required
def list_archived_forums(request):
    context = get_context()
    forums = models.Forum.objects.filter(is_listed=True, is_archived=True)
    context['list_name'] = 'Archiv'
    context['forums'] = forums
    return render(request, 'lantern/list-forums.html', context) 

