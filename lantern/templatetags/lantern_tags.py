from django import template
from django.utils import timezone

register = template.Library()

@register.simple_tag
def not_seen_for_days(user, days):
    if not user.last_login:
        return False
    delta = timezone.now() - user.last_login
    return delta.days > days


@register.filter
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()


